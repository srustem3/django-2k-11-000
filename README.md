# django-2k-11-000

## Установка и запуск проекта

- `python3 -m venv .venv` - создать виртуальное окружение
- `source .venv/bin/activate` - войти в виртуальное окружение
- `pip install -r requirements.txt` - установить зависимости
- `pre-commit install` - установка pre-commit хуков для запуска линтеров перед коммитом
- `docker-compose up` - поднять базу данных PostgreSQL (если Вы не используете Docker, установите PostgreSQL 
с официального сайта)
- `python manage.py migrate` - применить миграции к базе данных
- `python manage.py runserver` - запуск сервера для разработки
- `./celery.sh` - запускает воркеры очереди сообщений (celery)
- `celery -A testdjango flower` - запускает инструмент мониторинга очереди сообщений (celery flower)
