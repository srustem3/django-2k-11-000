from rest_framework import serializers
from drf_yasg.utils import swagger_serializer_method

from web.models import Site, User, SiteHistory


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()


class UserSerializer(serializers.ModelSerializer):
    has_telegram_account = serializers.SerializerMethodField(
        help_text="true - есть телеграм аккаунт, " "false - нет телеграм аккаунта",
    )

    @swagger_serializer_method(serializers.BooleanField())
    def get_has_telegram_account(self, instance: User):
        return instance.telegram_user_id is not None

    class Meta:
        model = User
        fields = ("id", "email", "has_telegram_account")
        extra_kwargs = {"email": {"help_text": "Электронная почта"}}


class LoginResponseSerializer(serializers.Serializer):
    token = serializers.CharField()
    user = UserSerializer()


class SiteSerializer(serializers.ModelSerializer):
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    def validate_url(self, url):
        return url.split("?")[0]

    def create(self, validated_data):
        validated_data["user_id"] = validated_data["user_id"].id
        return super(SiteSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        if "user_id" in validated_data:
            validated_data["user_id"] = validated_data["user_id"].id
        return super(SiteSerializer, self).update(instance, validated_data)

    class Meta:
        model = Site
        fields = (
            "id",
            "name",
            "url",
            "user_id",
            "created_at",
            "updated_at",
        )
        extra_kwargs = {"status": {"read_only": True}}


class SiteViewSerializer(SiteSerializer):
    user = UserSerializer(read_only=True)
    history_count = serializers.IntegerField()
    history_success_count = serializers.IntegerField()
    history_success_percent = serializers.FloatField()

    class Meta:
        model = SiteSerializer.Meta.model
        fields = (
            *SiteSerializer.Meta.fields,
            "user",
            "status",
            "history_count",
            "history_success_count",
            "history_success_percent",
        )
        extra_kwargs = SiteSerializer.Meta.extra_kwargs


class SiteHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteHistory
        fields = ("id", "is_success", "status_code", "created_at")


class SiteStatSerializer(serializers.Serializer):
    sites_count = serializers.IntegerField()
