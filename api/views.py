from django.contrib.auth import authenticate
from django.db.models import Count
from django.utils.decorators import method_decorator
from rest_framework import mixins, status, permissions
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, action, permission_classes, authentication_classes
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from drf_yasg.utils import swagger_auto_schema

from api.permissions import BlockListPermission
from api.serializers import (
    SiteSerializer,
    SiteHistorySerializer,
    SiteStatSerializer,
    LoginSerializer,
    LoginResponseSerializer,
    SiteViewSerializer,
)
from web.models import Site
from web.services.sites import check_sites


@swagger_auto_schema(method="get", operation_id="api_status")
@api_view()
def main_api(request):
    """Метод для проверки работы API"""
    return Response({"status": "ok"})


@swagger_auto_schema(
    method="post",
    request_body=LoginSerializer,
    responses={
        200: LoginResponseSerializer(),
        400: "Ошибки в передаваемых на сервер данных",
        403: "Некорректные учетные данные",
    },
    tags=["auth"],
)
@api_view(["POST"])
@permission_classes([])
def login_view(request):
    """Вход"""
    serializer = LoginSerializer(data=request.data)
    serializer.is_valid(True)

    email = serializer.validated_data["email"]
    password = serializer.validated_data["password"]
    user = authenticate(request, email=email, password=password)
    if user is None:
        raise AuthenticationFailed

    token_model, created = Token.objects.get_or_create(user=user)

    response_serializer = LoginResponseSerializer({"token": token_model.key, "user": user})
    return Response(response_serializer.data)


@method_decorator(name="create", decorator=swagger_auto_schema(tags=["sites"]))
@method_decorator(name="retrieve", decorator=swagger_auto_schema(tags=["sites"]))
@method_decorator(name="update", decorator=swagger_auto_schema(tags=["sites"]))
@method_decorator(name="partial_update", decorator=swagger_auto_schema(tags=["sites"]))
@method_decorator(name="list", decorator=swagger_auto_schema(tags=["sites"]))
class SitesView(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """Сайты"""

    permission_classes = [permissions.IsAuthenticated, BlockListPermission]

    def get_serializer_class(self):
        if self.action in ("create", "update", "partial_update"):
            return SiteSerializer
        if self.action in ("list", "retrieve"):
            return SiteViewSerializer

    def get_queryset(self):
        user = self.request.user
        return Site.objects.all().select_related("user").annotate_history_count().filter(user=user)

    @swagger_auto_schema(method="post", responses={204: "Запущена проверка сайта"}, tags=["sites"])
    @action(["POST"], detail=True)
    def check(self, request, *args, **kwargs):
        """Проверить работу сайта"""
        object = self.get_object()
        check_sites([object.id])
        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(method="post", responses={204: "Запущена проверка всех сайтов"}, tags=["sites"])
    @action(["POST"], detail=False)
    def check_all(self, request, *args, **kwargs):
        """Проверить работу всех сайтов"""
        check_sites()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(method="get", responses={200: SiteHistorySerializer(many=True)}, tags=["sites"])
    @action(["GET"], detail=True)
    def history(self, request, *args, **kwargs):
        """Получить историю проверки сайта"""
        object: Site = self.get_object()
        history_items = object.history.all().order_by("-created_at")
        serializer = SiteHistorySerializer(history_items, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(method="get", responses={200: SiteStatSerializer()}, tags=["sites"])
    @action(["GET"], detail=False)
    def stat(self, request, *args, **kwargs):
        """Получить статистику по проверкам"""
        result = self.get_queryset().aggregate(Count("id"))
        serializer = SiteStatSerializer(
            {
                "sites_count": result["id__count"],
            }
        )
        return Response(serializer.data)
