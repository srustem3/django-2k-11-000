from datetime import datetime

from django.shortcuts import render


def main_view(request):
    context = {"current_time": datetime.now()}
    return render(request, "web/main.html", context)


def html_view(request):
    return render(request, "web/html.html")
