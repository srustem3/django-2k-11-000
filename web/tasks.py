from testdjango.celery import app
from web.services.notifications import send_notifications


@app.task(queue="default")
def send_notifications_task(users_notification_map: dict):
    send_notifications(users_notification_map)


@app.task(queue="default")
def check_sites_task():
    from web.services.sites import check_sites

    check_sites()
