from django.contrib import admin, messages

from web.models import SiteHistory
from web.services.sites import check_sites


class SiteHistoryInline(admin.TabularInline):
    model = SiteHistory
    readonly_fields = ("status_code", "error_response_content", "created_at")

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class HttpsFilter(admin.SimpleListFilter):
    title = "Протокол сайта"
    parameter_name = "https"

    def has_output(self):
        return True

    def lookups(self, request, model_admin):
        return ((True, "С HTTPS"), (False, "Без HTTPS"))

    def queryset(self, request, queryset):
        qs = queryset

        if self.value():
            if self.value() == str(True):
                qs = qs.filter(url__startswith="https://")
            elif self.value() == str(False):
                qs = qs.filter(url__startswith="http://")
        return qs


def check_sites_action(modeladmin, request, queryset):
    site_ids = queryset.values_list("id", flat=True)
    if len(site_ids) > 0:
        check_sites(site_ids)
        modeladmin.message_user(request, f"{len(site_ids)} сайтов проверено.", messages.SUCCESS)


class SiteModelAdmin(admin.ModelAdmin):
    list_display = ("get_site_full_name", "status", "created_at", "updated_at")
    list_display_links = ("get_site_full_name",)
    list_filter = ("status", "created_at", HttpsFilter)
    search_fields = ("id", "name", "url")
    readonly_fields = ("get_site_full_name", "status", "created_at", "updated_at")
    ordering = ("-updated_at",)
    inlines = (SiteHistoryInline,)
    actions = (check_sites_action,)
    change_form_template = "web/admin/site_change_form.html"

    check_sites_action.short_description = "Проверить сайты"

    def get_site_full_name(self, instance):
        return f"#{instance.id} {instance.name} ({instance.url})"

    get_site_full_name.short_description = "Полное название сайта"

    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        if "_check_site" in request.POST:
            check_sites([object_id])
            self.message_user(request, "Сайт проверен", messages.SUCCESS)
        return super(SiteModelAdmin, self).changeform_view(request, object_id, form_url, extra_context)

    class Media:
        css = {"all": ("web/css/admin.css",)}
